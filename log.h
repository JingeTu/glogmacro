#pragma once

#include <glog/logging.h>

//#define LOGHEAD __FILE__ << ":"<< __LINE__ << "(" << __func__ << "): "
#define LOGHEAD "(" << __func__ << "): "

#define LOGi LOG(INFO)    << LOGHEAD
#define LOGw LOG(WARNING) << LOGHEAD
#define LOGe LOG(ERROR)   << LOGHEAD
#define LOGf LOG(FATAL)   << LOGHEAD

#define LOGVARi(var) do {LOGi << "`"#var"`'s value: " << var;} while (0)
#define LOGVARw(var) do {LOGw << "`"#var"`'s value: " << var;} while (0)
#define LOGVARe(var) do {LOGe << "`"#var"`'s value: " << var;} while (0)
#define LOGVARf(var) do {LOGf << "`"#var"`'s value: " << var;} while (0)

#define CHECKf(condition) LOG_IF(FATAL, GOOGLE_PREDICT_BRANCH_NOT_TAKEN(!(condition))) << LOGHEAD << "CHECK(" #condition ") failed."

#define GET_MACRO2(_1, _2, NAME, ...) NAME
#define GET_MACRO3(_1, _2, _3, NAME, ...) NAME

#define CHECKeBool(condition) do {if (!(condition)) {LOGe << "CHECK("#condition") failed."; return false;}} while(0)
#define CHECKeVar(condition, ret) do {if (!(condition)) {LOGe << "CHECK("#condition") failed."; return ret;}} while(0)
#define CHECKe(...) GET_MACRO2(__VA_ARGS__, CHECKeVar, CHECKeBool) (__VA_ARGS__)

#define CHECKeoBool(condition, output) do {if (!(condition)) {LOGe << "CHECK("#condition") failed. DETAIL: '" << output << "'."; return false;}} while(0)
#define CHECKeoVar(condition, ret, output) do {if (!(condition)) {LOGe << "CHECK("#condition") failed. DETAIL: '" << output << "'."; return (ret);}} while(0)
#define CHECKeo(...) GET_MACRO3(__VA_ARGS__, CHECKeoVar, CHECKeoBool) (__VA_ARGS__)

#define CHECKCodeeDef(condition) do {int ret = (condition); if (ret != 0) {LOGe << "CHECK(" #condition ") failed."; return ret;}} while(0)
#define CHECKCodeeVar(condition, val) do {int ret = (condition); if (ret != 0) {LOGe << "CHECK(" #condition ") failed."; return (val);}} while(0)
#define CHECKCodee(...) GET_MACRO2(__VA_ARGS__, CHECKCodeeVar, CHECKCodeeDef) (__VA_ARGS__)

#define CHECKCodeeoDef(condition, output) do {int ret = (condition); if (ret != 0) {LOGe << "CHECK(" #condition ") failed. DETAIL: '" << output << "'."; return ret;}} while(0)
#define CHECKCodeeoVar(condition, val, output) do {int ret = (condition); if (ret != 0) {LOGe << "CHECK(" #condition ") failed. DETAIL: '" << output << "'."; return (val);}} while(0)
#define CHECKCodeeo(...) GET_MACRO3(__VA_ARGS__, CHECKCodeeoVar, CHECKCodeeoDef) (__VA_ARGS__)
