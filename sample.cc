#include "log.h"
#include <gflags/gflags.h>
#include <fstream>

int foo2() {
    std::string filename = "/tmp/this_file_do_not_exists";
    std::ifstream file(filename);

    CHECKeo(file.is_open(), 1, "Add some detail here, file " << filename << " does not exist.");

    file.close();
    return 0;
}

int foo1() {
    CHECKe(foo2() == 0, 1); /// If check failed, return 1.
    return 0;
}

bool foo0() {
    CHECKe(foo1() == 0);
    return true;
}

int main(int argc, char* argv[]) {

    google::InitGoogleLogging(argv[0]);

    gflags::ParseCommandLineFlags(&argc, &argv, true);

    FLAGS_alsologtostderr = 1;

    LOGi << "Will output function name `main` alongside default glog output.";

    int number = 101;

    LOGi << "This will save the time of typing variable name";
    LOGVARi(number);

    CHECKf(foo0()); /// If check failed, fatal exit.

    return 0;
}
